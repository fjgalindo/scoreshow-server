<?php
return [
    'adminEmail' => 'admin@example.com',
    'api' => [
        'v1' => [
            'comment_default_visibility' => 1,
            'timeToRefeshCache' => "+1 week"
        ]
    ]
];
